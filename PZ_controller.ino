#include <RP2040_PWM.h>

#define PIL_P 12 //odbiornik CH1
#define PIL_S 11 //odbiornik CH2
#define PIL_LP 9 //odbiornik CH3
#define PIL_PT 10 //odbiornik CH4

//#define M_L 8
//#define M_R 7

#define M_L 20 //sprzetowy PWM enumeruje piny wg wyprowadzen procesora, nie wg arduino
#define M_R 19

#define SERVO 6
#define PUMP 5

#define BIAS 5
#define PWM_FREQ 2000

RP2040_PWM* PWM_L;
RP2040_PWM* PWM_R;

void setup() 
{
  pinMode(PIL_P, INPUT);  //wejscia z pilota
  pinMode(PIL_S, INPUT);
  pinMode(PIL_LP, INPUT);
  pinMode(PIL_PT, INPUT);
  
  //pinMode(M_L, OUTPUT); //wyjscia na silniki, serwa i LED
  //pinMode(M_R, OUTPUT);

  PWM_L = new RP2040_PWM( M_L, PWM_FREQ, 0, true);
  PWM_L->setPWM();
  PWM_R = new RP2040_PWM(M_R, PWM_FREQ, 0, true);
  PWM_R->setPWM();
  
  pinMode(SERVO, OUTPUT);
  pinMode(PUMP, OUTPUT);
  pinMode( LED_BUILTIN, OUTPUT);

  Serial.begin(57600); //diagnostyka przez serial
  Serial.println("goodbye\n");
}

void drive( int power, int dir )
{
  char msg[256];
  
  int power_l = power - dir;
  int power_r = power + dir;

  if( power_l > 255 ) power_l = 255;
  if( power_r > 255 ) power_r = 255;
  if( power_l < 10 ) power_l = 0;
  if( power_r < 10 ) power_r = 0;

  sprintf( msg, "motorki: P: %d\tL: %d", power_r*400, power_l*400);
  Serial.println( msg); //komunikacja
  
  //analogWrite( M_R, power_r );
  //analogWrite( M_L, power_l );
  PWM_L->setPWM_Int( M_L, PWM_FREQ, power_l*400, true);
  PWM_R->setPWM_Int( M_R, PWM_FREQ, power_r*400, true);
}

void loop() 
{
  char msg[1024];
  int lp_map;
  int pt_val, lp_val, p_val, s_val, pt_map, p_map;
  
  pt_val = pulseIn( PIL_PT, HIGH, 30000); //odczyt z pilota
  lp_val = pulseIn( PIL_LP, HIGH, 30000);
  p_val = pulseIn( PIL_P, HIGH, 300000);
  s_val = pulseIn( PIL_S, HIGH, 300000);

  if( pt_val == 0 || lp_val == 0 || p_val == 0 || s_val == 0 )
  {
    drive( 0, 0);
    analogWrite( PUMP, 0);
    Serial.println("Brak sygnalu z odbiornika");
    digitalWrite( LED_BUILTIN, 0);
  }
  else
  {
    pt_map = map( pt_val, 1000, 1990, 0, 255 ); //mapowanie
    lp_map = map( lp_val+BIAS, 1000, 2000, -128, 127 );
    p_map = map( p_val, 1200, 1800, 0, 255 );

    if( p_map > 255 ) p_map = 255;
    if( p_map < 10 ) p_map = 0;
  
    sprintf( msg, "raw: PT: %u\tLP: %u\tS: %u\tP: %u\t\tmap: S: %u\tP: %u", pt_val, lp_val, s_val, p_val, s_val/8, p_map);
    Serial.println( msg); //komunikacja

    analogWrite( SERVO, s_val/8); //wystawienie na wyjscia
    analogWrite( PUMP, p_map);
    drive( pt_map, lp_map);

    digitalWrite( LED_BUILTIN, 1);
  }
}
